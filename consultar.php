<?php include("template/cabecera.php") ?>
<?php include_once 'conexion.php';?>

<div class="jumbotron">
    <h1 class="display-3">Consultar Estudiantes</h1>
    <hr class="my-2">
    <p>Codigo: nombre apellido - fecha nacimiento</p>
    
</div>

<?php
$sql_leer = 'SELECT * FROM estudiante';

$gsent = $pdo->prepare($sql_leer);

$gsent->execute();
$resultado = $gsent->fetchall();

 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="container mt-5">
        <div class="colmd-6">
            
            <?php foreach($resultado as $dato): ?>

            <div 
            class="alert alert-primary" 
            role="alert">
            <?php echo $dato['codigo']?>
            :
            <?php echo $dato['nombre']?>
            <?php echo $dato['apellido']?>
            -
            <?php echo $dato['fecha_nacimiento']?>
            </div>

            <?php endforeach ?>
            
        </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </div>
  </body>
</html>


<?php include ("template/pie.php")?>